function Plane() {
    tPlane = new Sprite(scene, "./img/plane2.png", 70, 70);
    tPlane.setSpeed(0);
    tPlane.setPosition()
    tPlane.lastFireTime = 0;
    tPlane.keyHeld = false;

    tPlane.followMouse = function () {
        this.setPosition(scene.getMouseX(), 700);
    }
    tPlane.checkClick = function () {
        if (this.isClicked()) {
            missile.fire();
            console.log("Click");
        }
    } // end if
    tPlane.checkKeys = function () {
        if (keysDown[K_SPACE]) {
            this.fireMissile();
        }
        // Check mouse click event
        scene.canvas.addEventListener('mousedown', function () {
            tPlane.fireMissile();
        });

        this.fireMissile = function () {
            currentTime = new Date().getTime();
            if (!this.keyHeld && currentTime - this.lastFireTime >= 500) {
                missile.fire();
                this.lastFireTime = currentTime;
                this.keyHeld = true;
            } else {
                this.keyHeld = false;
            }
        }
    }
    return tPlane;
} // end plane constructor

function Boss() {
    tBoss = new Sprite(scene, "./img/boss.png", 150, 100);
    tBoss.setSpeed(0);
    tBoss.lastFireTime = 0;
    tBoss.name = "";
    tBoss.timerShoot = (Math.floor(Math.random() * 5) + 1)*1000;
    tBoss.currentTime = 0;
    tBoss.life = true;
    // tBoss.init = true;
    // setTimeout(() => {
    //     tBoss.init = false;
    // }, 500);
    tBoss.reset = function () {
        this.changeImage("./img/boss.png");
        this.life = true;
        this.setDY((Math.random() * BG_SPEED) + 5);
        this.setDX((Math.random() * 10) - 5);
        newX = Math.random() * scene.width;
        this.setPosition(newX, 10);
    } // end reset

    tBoss.checkBounds = function () {
        if (this.y > scene.height) {
            this.reset();
        } // end if
    } // end checkBounds

    setTimeout(() => {
        tBoss.init = false;
    }, 0);

    tBoss.dead = function () {
        this.isDead = true;
    }
    tBoss.fire = function (boss,missile) {
        this.currentTime = new Date().getTime();
        // && this.isDead==false&&this.init==false
        if (this.currentTime - this.lastFireTime >= this.timerShoot) {
            console.log(boss.name);
            this.lastFireTime = this.currentTime;
            missile.fire(boss);
            // new MissileBoss().fire(boss);
        }


    }
    tBoss.reset();

    return tBoss;
} // end cloud 

function makeBoss() {
    boss = new Array(NUM_BOSS);
    missileAll = new Array(NUM_BOSS);
    for (i = 0; i < NUM_BOSS; i++) {
        missileAll[i] = new MissileBoss();
        missileAll[i].name = "missile: "+(i+1)
        boss[i] = new Boss();
        boss[i].name = "บอสตัวที่: "+(i+1)
    } // end for

} // end makeBoss



function Island() {
    tIsland = new Sprite(scene, "./img/kiki.png", 100, 100);
    tIsland.lastFireTime = 0;
    tIsland.timerShoot = Math.floor(Math.random() * 5) + 1
    tIsland.name= "isLand";
    tIsland.life = true;
    tIsland.setSpeed(0);
    tIsland.reset = function () {
        this.setDY(20);
        this.setDX(0);
        newX = Math.random() * scene.width;
        this.setPosition(newX,-10);
    } // end reset

    tIsland.checkBounds = function () {
        if (this.y > scene.height) {
            this.reset();
        } // end if
    } // end checkBounds
    tIsland.fire = function () {

        var currentTime = new Date().getTime();
        if (currentTime - this.lastFireTime >= 3000) {
            // console.log("Shoot");
            this.lastFireTime = currentTime;
            // missileBoss.fire();
        }
    }

    tIsland.reset();

    return tIsland;
} // end island

function Missile() {
    tMissile = new Sprite(scene, "./img/bulet.png", 70, 30);
    tMissile.hide();
    let x = 50;
    tMissile.fire = function () {
        this.show();
        soundMissile = new Audio("./sound/laser2.mp3");
        soundMissile.play();
        this.setSpeed(x);
        this.setBoundAction(DIE);
        this.setPosition(plane.x, plane.y);
        this.setAngle(plane.getImgAngle() + 270);
    } // end fire
    return tMissile;
} // end missile def

function MissileBoss() {
    tMissile = new Sprite(scene, "./img/bulletEnemy.png", 70, 30);
    tMissile.hide();
    tMissile.name  = "";
    tMissile.life = true;
    tMissile.reset = function () {
      this.changeImage("./img/bulletEnemy.png")
    } // end
    let x = 50;

    tMissile.fire = function (boss) {
        this.show();
        soundMissile = new Audio("./sound/laser2.mp3");
        soundMissile.play();
        this.setSpeed(x);
        this.setBoundAction(DIE);
        this.setPosition(boss.x, boss.y);
        this.setAngle(boss.getImgAngle() + 90);
        console.log("ยิง")
    } // end fire
    return tMissile;
} // end missile def

function Background() {
    tBackground = new Sprite(scene, "./img/BG2.png", 3000, 1800);
    tBackground.setDX(0);
    scene.setSize(1880, 800)
    tBackground.setDY(BG_SPEED);
    tBackground.setPosition(100, 1000);

    tBackground.checkBounds = function () {
        if (this.y > 720) {
            this.setPosition(400, -120)
        } // end if
    } // end checkBounds

    tBackground.checkKeys = function () {
        if (keysDown[K_UP]) {
            this.changeYby(-1);
        }
        if (keysDown[K_DOWN]) {
            this.changeYby(1);
        }
        console.log("Background Y value: " + this.y);
    } // end checkKeys

    return tBackground;
} // end ocean